/*
  SENSORVIVERO
 */

#include <SPI.h>
#include <Ethernet.h>
#include <Time.h>
#include <DHT.h>
#include <avr/pgmspace.h>

// Numero de serie unico
static const char NUM_SERIE[12] = "\"SVP-0001\"";

// Puertos de los LEDS, Reemplazan al subsistema correspondiente
#define riego 8
#define luz 9
#define tempAlert 7

// Puertos de sensores
#define senHumTemp 2
#define senLum A1

// Estado actual sistemas
unsigned char riegoActivo = 0;
unsigned char luzActiva = 0;
unsigned char tempActiva = 0;

// Valores sensores
int humedad;
int luminosidad;
int temperatura;

// Valores limites
#define DURACION_PULSO_RIEGO 5000
unsigned int maxPulsosRiego = 6;
unsigned int pulsosAcumulados;
unsigned int humMin;
unsigned int humMax;

unsigned long fotoPeriodo;
unsigned long luzAcumulada;
unsigned int lumMin;
unsigned int lumMax;
float lumHist;

int tempMin;
int tempMax;

// Reset valores
unsigned int hora_reset;
unsigned int minutos_reset;
time_t resetValoresDate;

// Control de gestion
#define MAX_ARGUMENTS 3
const char MODE_AUTO[3] = "x";
char mode[3];
const unsigned long INTERVALO_MEDIDA = 10000;
long lastMeasure;

// Control envio informacion
const unsigned long INTERVALO_ENVIO = 20000;
long lastEnvio;
const unsigned long INTERVALO_REINTENTO_ENV = 30000;
long lastIntentoEnvio;

// Variables para la gestion de red
byte mac[] = { 
  0x90, 0xA2, 0xDA, 0x00, 0x1F, 0xCB };
IPAddress ip(192, 168, 1, 148); // IP address, may need to change depending on network

//ARDUINO actuando como cliente
EthernetClient client;
const int SERVER_PORT = 3000;
IPAddress serverIP(192, 168, 1, 4);

//DHT
#define DHTTYPE DHT11

// Time
unsigned int timePort = 8888;      // local port to listen for UDP packets
IPAddress timeServer(89, 248, 106, 98); // NTP server pool.ntp.org or time.nist.gov
const int NTP_PACKET_SIZE= 48; // NTP time stamp is in first 48 bytes of message
byte packetBuffer[ NTP_PACKET_SIZE]; // buffer to hold incoming/outgoing packets
EthernetUDP Udp; // A UDP instance to let us send and receive packets over UDP

/*
//ARDUINO actuando como servidor
 //Si se habilitar, buscar todos los comentarios relacionados con estas variables y descomentar
 
 boolean modoServidor = false; //Para habilitar el modo servidor
 const int PORT = 80;
 EthernetServer server(PORT);  // create a server at port 80
 
 const char CODE_OK[20] = "HTTP/1.1 200 OK";
 const char CODE_FORBIDDEN[25] = "HTTP/1.1 403 Forbidden";
 const char CODE_BAD_REQ[27] = "HTTP/1.1 400 Bad Request";
 const int RESP_CODE_SIZE = 30;
 char HTTP_resp_code[RESP_CODE_SIZE]; 
 */

// Cabecera de la petición al servidor
prog_char HTTP_FORM_METHOD[] PROGMEM = "POST ";
prog_char HTTP_SERVER_URL[] PROGMEM = "/json/datos";
prog_char HTTP_PROTOCOL[] PROGMEM = " HTTP/1.1\r\n";
prog_char HTTP_CON_CLOSE[] PROGMEM = "Connection: close\r\n";
prog_char HTTP_CONTENT_TYPE[] PROGMEM = "Content-Type: application/json\r\n";
prog_char HTTP_CONTENT_LENGTH[] PROGMEM = "Content-Length: ";

PROGMEM const char *request[] =
{
  HTTP_FORM_METHOD,
  HTTP_SERVER_URL,
  HTTP_PROTOCOL,
  HTTP_CON_CLOSE,
  HTTP_CONTENT_TYPE,
  HTTP_CONTENT_LENGTH
};

// Buffers para la recepción y envío de las peticiones
#define FROM_SIZE 175
#define TO_SIZE 175
char HTTP_from_server[FROM_SIZE]; // stores the HTTP from server
char HTTP_to_server[TO_SIZE]; // stores the HTTP to server
int bufIndex;

// Errores
prog_char ERROR_BAD_SYNTAX[] PROGMEM = "{\"e\":\"error\",\"v\":\"ErrBadSyntax\"},";
prog_char ERROR_OVERFLOW[] PROGMEM = "{\"e\":\"error\",\"v\":\"ErrBuffOverflow\"},";
prog_char ERROR_FORBIDDEN[] PROGMEM = "{\"e\":\"error\",\"v\":\"ErrForbidden\"},";
prog_char ERROR_UNKNOWN[] PROGMEM = "{\"e\":\"error\",\"v\":\"ErrUnknow\"},";
prog_char ERROR_BAD_ACTION[] PROGMEM = "{\"e\":\"error\",\"v\":\"ErrBadAction\"},";

PROGMEM const char *errores[] =
{
  ERROR_BAD_SYNTAX,
  ERROR_OVERFLOW,
  ERROR_FORBIDDEN,
  ERROR_UNKNOWN,
  ERROR_BAD_ACTION
};

//API
const char APIHEADER[11] = "acc:"; //Palabra clave para comenzar el procesado
const char ACTION[2] = "a";
const char VALUE1[3] = "v1";
const char VALUE2[3] = "v2";

#define SWITCHMODE 0
#define GETDATA 1
#define ENCENDERRIEGO 2
#define ENCENDERLUZ 3
#define ENCENDERTEMP 4
#define APAGARRIEGO 5
#define APAGARLUZ 6
#define APAGARTEMP 7
#define GETLUMMINMAX 8
#define GETTEMPMINMAX 9
#define GETHUMMINMAX 10
#define GETMAXPULSOSRIEGO 11
#define GETLUMHIST 12
#define GETFOTOPERIODO 13
#define GETHORARESET 14
#define SETLUMMINMAX 15
#define SETTEMPMINMAX 16
#define SETHUMMINMAX 17
#define SETMAXPULSOSRIEGO 18
#define SETLUMHIST 19
#define SETFOTOPERIODO 20
#define SETHORARESET 21
#define GETNUMSERIE 22
#define DELAY 23
#define NOP 24
#define MODEAUTOISON 40

//INFORMACION A SERVIDOR
const char INIT_INFO[22] = "{\"info\":{\"num_serie\":";
const char ESTADO[12] = ",\"estado\":[";

const char ELEMENTO[12] = "\"e\":";
const char VALOR[10] = ",\"v\":";

const char NUEVOMODO[12] = "\"NuevoModo\"";
const char NUMSERIE[11] = "\"NumSerie\"";
const char LUMINOSIDAD[14] ="\"Luminosidad\"";
const char HUMEDAD[10] = "\"Humedad\"";
const char TEMPERATURA[14] = "\"Temperatura\"";

const char RIEGOACTIVO[14] = "\"RiegoActivo\"";
const char LUZACTIVA[12] = "\"LuzActiva\"";
const char TEMPACTIVA[18] = "\"TempActiva\"";

const char LUMMIN[9] = "\"LumMin\"";
const char LUMMAX[9] = "\"LumMax\"";
const char HUMMIN[9] = "\"HumMin\"";
const char HUMMAX[9] = "\"HumMax\"";
const char TEMPMIN[10] = "\"TempMin\"";
const char TEMPMAX[10] = "\"TempMax\"";

const char MAXPULSOSRIEGO[17] = "\"MaxPulsosRiego\"";
const char LUMHIST[10] = "\"LumHist\"";
const char FOTOPERIODO[14] = "\"FotoPeriodo\"";
const char HORARESET[12] = "\"HoraReset\"";
const char MINUTOSRESET[15] = "\"MinutosReset\"";

// Auxiliares
const int SEGS_POR_MINUTO = 60;
const int SEGS_POR_HORA = 3600;
const int SEGS_POR_DIA = 86400;

const char INIT_RESP[2] = "{";
const char END_RESP[2] = "}";
const char COMILLAS[2] = "\"";
const char COMA[2] = ",";
const char ESPACIO = ' ';
const char DOSPUNTOS[2] = ":";
const char RETORNO = '\r';
const char NLINEA = '\n';
const char LLA = '{';
const char LLC = '}';
const char CA = '[';
const char CC = ']';
const char CCS[2] = "]";
const char NULLCHAR = '\0';
const char NULLS[5] = "null";

DHT dht(senHumTemp, DHTTYPE);

// Se lanza al iniciar por primera vez
void setup() {   
  // Inicializamos la consola serial
  Serial.begin(9600);

  Serial.println(F("Setup init."));

  // Inicializamos los subsistemas como output
  pinMode(riego, OUTPUT);     
  pinMode(luz, OUTPUT);
  pinMode(tempAlert, OUTPUT);
  apagarRiego();
  apagarLuz();
  apagarTemp();

  //avisoReinicio();

  // Inicializamos los puertos de los sensores
  pinMode(senLum, INPUT);

  // Modo por defecto AUTO
  strcpy(mode, MODE_AUTO);

  // Preparamos la gestion de red
  Ethernet.begin(mac,  ip);
  bufIndex = 0;

  // Esperamos un tiempo a que todo se complete
  delay(1000);

  // Inicializamos valores
  inicializaValores(); 

  // Inicializamos el sensor temp/hum
  dht.begin();

  /*
  // (Des)Activamos el servidor
   modoServidor = false;
   if(modoServidor)
   {
   server.begin();
   }
   */

  delay(2000);

  //Serial.println(F("Setup end."));
}

//void avisoReinicio()
//{
//  // Hace parpadear los leds 5 veces para avisar de un reinicio
//  // Solo debera usarse en modo debug
//  for(int i = 0; i < 5; i++)
//  {
//    encenderRiego();
//    encenderTemp();
//    encenderLuz();
//    delay(500);
//    apagarRiego();
//    apagarTemp();
//    apagarLuz();
//    delay(500);
//  }
//}

// MAIN
void loop() 
{
  long currentms = millis();
  long tiempoms = currentms-lastMeasure; 
  if(tiempoms > INTERVALO_MEDIDA)
  {  
    // Si el tiempo desde la última medida es mayor que el intervalo establecido
    // Medimos y gestionamos

    long dif = now() - resetValoresDate;    
    if(dif > 0)
    {
      // resetValoresDate contiene la fecha y hora del proximo reset
      // Si hemos pasado de esa fecha, reiniciamos valores
      resetValoresAcumulados();
    }

    leerValores(); // Leemos los valores de los sensores
    lastMeasure = millis(); // Actualizamos la marca de tiempo de la última medida
    gestion(tiempoms); // Gestionamos los subsistemas
  }

  if(currentms-lastEnvio > INTERVALO_ENVIO && currentms-lastIntentoEnvio > INTERVALO_REINTENTO_ENV)
  {
    // Si el tiempo desde el último envío es mayor que el intervalo de envío
    // Y si además, ante un fallo, ha pasado suficiente para reintentar el envío
    // Enviamos.
    boolean res = enviarInformacion(); // Envio de la primera request.
    lastIntentoEnvio = millis(); // Actualizamos la marca de tiempo del último intento envío.
    if(res)
    {
      // Si el envío es correcto.
      lastEnvio = millis(); // Actualizamos la marca de tiempo del último envío.
      delay(12000); //Le damos tiempo al servidor para responder
      res = atenderRespuesta(); // Atendemos la respuesta del servidor
      res = procesarYContestar(res); // Ejecutamos las acciones pedidas y contestamos con el resultado
      if(res)
      {
        delay(12000); //Le damos tiempo al servidor para responder
        consumirRespuesta();
      }   

      delay(5000);
    }
  }

  Serial.println(freeRam());

  /*
  if(modoServidor)
   { //Atendemos la red solo si el modo servidor esta activo.
   atenderRed(); 
   }
   */

  delay(2000);
}

void inicializaValores()
{
  // Iniciamos algunos valores por defecto
  resetValoresAcumulados();

  lastMeasure = - INTERVALO_MEDIDA;
  lastEnvio = - INTERVALO_ENVIO;
  lastIntentoEnvio = -INTERVALO_REINTENTO_ENV;

  maxPulsosRiego = 6;
  pulsosAcumulados = 0;
  humMin = 37;
  humMax = 90;

  fotoPeriodo = 18L * 1000L * (long) SEGS_POR_HORA;
  luzAcumulada = 0;
  lumMin = 980;
  lumMax = 980;
  lumHist = 0.01;

  tempMin = 10;
  tempMax = 38;
}

void resetValoresAcumulados()
{
  // Reset de valores
  Serial.println(F("Reseteamos los valores acumulados."));
  pulsosAcumulados = 0;
  luzAcumulada = 0;

  if(timeStatus() == timeNotSet || timeStatus() == timeNeedsSync){
    // Sincronizacion del reloj si es necesario
    Udp.begin(timePort);
    setSyncProvider(getNtpTime);
    while(timeStatus()== timeNotSet|| timeStatus() == timeNeedsSync); // wait until the time is set by the sync provider
  }

  // El próximo reinicio se calcula a partir de la medianoche de hoy y de la hora y minutos establecidos
  resetValoresDate = nextMidnight(now()) 
    + (hora_reset*SEGS_POR_HORA + minutos_reset*SEGS_POR_MINUTO)
      - (2*SEGS_POR_HORA); //Hora española
}

void leerValores()
{
  Serial.println(F("Leyendo informacion de los sensores..."));
  leeLuminosidad();
  leeHumTemp();
}

boolean enviarInformacion()
{
  Serial.println(F("Enviando datos al servidor..."));

  boolean res = false;

  HTTP_to_server[0] = NULLCHAR;

  // La peticion empieza con estructura constante, enviando numero de serie y estado del sistema
  strcat(HTTP_to_server, INIT_INFO);
  strcat(HTTP_to_server, NUM_SERIE);
  strcat(HTTP_to_server, ESTADO);

  //getDataReq copia al bufer HTTP_to_server el estado de los sensores y los subsitemas
  res = getDataReq();

  char *rem = HTTP_to_server; //Para quitar la coma final
  rem[strlen(rem)-1] = NULLCHAR;

  // Finalizamos la estructura JSON
  strcat(HTTP_to_server, CCS);
  strcat(HTTP_to_server, END_RESP);
  strcat(HTTP_to_server, END_RESP);

  if(!res)
  {
    // Si getDataReq devuelve false, algo ha fallado. Enviamos un error.
    HTTP_to_server[0] = NULLCHAR;
    strcat(HTTP_to_server, INIT_INFO);
    strcat(HTTP_to_server, NUM_SERIE);
    strcat(HTTP_to_server, ESTADO);

    char buffer[60];
    strcpy_P(buffer, (char*)pgm_read_word(&(errores[3])));
    strcat(HTTP_to_server, buffer);

    char *rem = HTTP_to_server; //Para quitar la coma final
    rem[strlen(rem)-1] = NULLCHAR;
    strcat(HTTP_to_server, CCS);
    strcat(HTTP_to_server, END_RESP);
    strcat(HTTP_to_server, END_RESP);
  }

  //Enviamos la peticion HTTP
  res = sendRequest();
  HTTP_to_server[0] = NULLCHAR;

  return res;
}

boolean sendRequest()
{
  boolean res = false;

  if(client.connect(serverIP, SERVER_PORT)) //Conexion con el servidor
  {
    // Si conseguimos conexión, preparamos la request

    //Tamaño de la petición
    int tam = strlen(HTTP_to_server);
    char buffer[35];

    //Cabecera de la petición, la recuperamos de la memoria con esta función especial. 
    //Ver las declaraciones de variables para más info.
    for(int i = 0; i <= 5; i++){
      strcpy_P(buffer, (char*)pgm_read_word(&(request[i])));
      client.print(buffer);
    }

    client.println(tam);
    client.println();

    // Una vez enviada la cabecera, procedemos a enviar el JSON
    client.println(HTTP_to_server);
    Serial.println(F("Enviando request..."));
    //Serial.println(HTTP_to_server);

    delay(500);
    res = true;
  }
  else {
    // Si no tenemos conexión al servidor, paramos y no continuamos hasta intento de reenvio.
    Serial.println(F("Conexion al servidor fallida."));
    HTTP_from_server[0]=NULLCHAR;
    HTTP_to_server[0]=NULLCHAR;
    bufIndex = 0; // index to 0
    res = false;
    client.stop();
  }

  return res;
}

boolean atenderRespuesta()
{
  Serial.println(F("Atendiendo respuesta..."));
  boolean currentLineIsBlank = true;
  boolean bodyrcv = false;
  int n = 0;
  boolean startrcv = false;
  boolean endrcv = false;

  HTTP_from_server[0] = NULLCHAR;
  bufIndex = 0;

  while (client.available()) 
  {
    // Mientras el servidor este conectado, leemos lo que tiene que enviarnos.
    char c = client.read();
    // Serial.print(c);

    // CONTROL //
    if(!bodyrcv && c == NLINEA && currentLineIsBlank)
    {
      // Si estamos en una linea en blanco y recibimos otro salto, el cuerpo de la petición va a empezar.
      bodyrcv = true;
    }
    // cada linea de la cabecera de la respuesta termina con \r\n
    else if (!bodyrcv && c == NLINEA) {
      // Empezamos una nueva linea
      currentLineIsBlank = true;
    } 

    else if (bodyrcv && c != RETORNO && !endrcv) {
      // Empezamos a recibir cuerpo de la petición
      // texto recibido desde el servidor
      if(c == LLA)
      {
        // Una llave abierta indica principio del JSON.
        n++; // Variable para controlar anidamiento de llaves.
        startrcv = true;
      }
      else if (c == LLC && startrcv)
      {
        // Una llave cerrada indica fin de una parte de la estructura JSON.
        n--;
        if(n == 0)
        {
          // Si no quedan mas llaves que cerrar, hemos recibido todo el JSON.
          endrcv = true;
        }
      }
    }

    // ALMACENAMIENTO //
    if(startrcv && !endrcv) 
    {
      // Se ha detectado el inicio del JSON y todavia no se ha recibido el final.
      //Estamos recibiendo la orden y la almacenamos
      if(bufIndex < FROM_SIZE) { // Control para evitar overflow
        // Serial.print(c);
        // En este caso no hay overflow
        if(c != '"' && c != RETORNO && c != NLINEA && c != '\t' && c != ESPACIO)
        {
          // Ignoramos los caracteres superfluos y almacenamos el resto
          HTTP_from_server[bufIndex] = c; 
          bufIndex++;
        }
      } 
      else {
        // Si hay overflow, enviamos error informando.
        HTTP_from_server[bufIndex]=NULLCHAR;
        while(client.read());  

        HTTP_to_server[0] = NULLCHAR;
        strcat(HTTP_to_server, INIT_INFO);
        strcat(HTTP_to_server, NUM_SERIE);
        strcat(HTTP_to_server, ESTADO);

        char buffer[60];
        strcpy_P(buffer, (char*)pgm_read_word(&(errores[1])));
        strcat(HTTP_to_server, buffer);

        char *rem = HTTP_to_server; //Para quitar la coma final
        rem[strlen(rem)-1] = NULLCHAR;
        strcat(HTTP_to_server, CCS);
        strcat(HTTP_to_server, END_RESP);
        strcat(HTTP_to_server, END_RESP);

        sendRequest();

        HTTP_from_server[0]=NULLCHAR;
        HTTP_to_server[0]=NULLCHAR;
        bufIndex = 0; // finished with request, index to 0
        break;
      }
    }
  }

  if (!client.connected()) 
  {
    // Paramos la conexión
    client.stop();
    client.flush();
  }
  else
  {
    Serial.println(F("Servidor aun conectado?"));
    delay(4000);
    // Este caso no debería darse, pero si se da esperamos y forzamos la parada.
    client.stop();
    client.flush();
  }

  HTTP_from_server[bufIndex]=NULLCHAR;
  bufIndex = 0;
  // true si la respuesta ha sido recibida completamente
  return bodyrcv && startrcv && endrcv;
}

boolean procesarYContestar(boolean syntaxCorrect)
{
  boolean res = false;

  // Inicio de la estructura JSON
  HTTP_to_server[0] = NULLCHAR;
  strcat(HTTP_to_server, INIT_INFO);
  strcat(HTTP_to_server, NUM_SERIE);
  strcat(HTTP_to_server, ESTADO);

  // Primer elemento es el num_serie, es el modo de comunicar ACK
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, NUMSERIE);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, NUM_SERIE);
  finalizaResp();

  Serial.println(HTTP_from_server);

  if(syntaxCorrect)
  {
    // Si la sintaxis recibida es correcta, empezamos a procesar acciones
    res = procesaAcciones(HTTP_from_server);
  }
  else
  {
    // Si la sintaxis es incorrecta, enviamos error.
    res = badSyntaxReq();
  }

  char *rem = HTTP_to_server; //Para quitar la coma final
  rem[strlen(rem)-1] = NULLCHAR;
  strcat(HTTP_to_server, CCS);
  strcat(HTTP_to_server, END_RESP);
  strcat(HTTP_to_server, END_RESP);

  delay(200);
  // Enviamos la respuesta que se ha ido construyendo conforme se ejecutaban las acciones
  sendRequest();

  // Si el envio se completa devolvemos true, para consumir la respuesta.
  res = true;

  HTTP_from_server[0]=NULLCHAR;
  HTTP_to_server[0]=NULLCHAR;
  bufIndex = 0; // finished with request, index to 0

    return res;
}

boolean consumirRespuesta()
{
  // Este metodo se encarga de consumir la respuesta a la segunda request.
  // No hacemos nada con ella, solo la recibimos para que la conexión se cierre correctamente.
  Serial.println(F("2a.Respuesta"));

  while (client.available()) 
  {
    client.read();
    delay(5);
  }

  delay(2000);

  if (!client.connected()) 
  {
    client.stop();
    client.flush();
  }
  else
  {
    Serial.println(F("Servidor aun conectado?"));
    delay(4000);
    client.stop();
    client.flush();
  }

  return true;
}

boolean procesaAcciones(char* acciones)
{
  //Acciones contiene todas las acciones que exige el servidor.
  if(acciones == NULL)
  {
    return false;
  }

  boolean res = false;
  boolean startrcv = false;
  boolean endrcv = false;
  char accion[32];
  int i = 0;

  boolean arrayStarted = false;
  boolean arrayEnded = false;

  //El tratamiento es similar a cuando se almacenaba la información del servidor.
  char *read = acciones;
  do{
    // Se espera un array de acciones
    if(!arrayStarted){
      if(*read == CA)
      { 
        // Si recibimos un corchete abierto puede indicar comienzo del array
        if(strstr(acciones, APIHEADER) != NULL){
          //Si encontramos la palabra clave "acciones", el array empieza.
          arrayStarted= true;
          accion[0] = NULLCHAR;
          i = 0;
        }
        else{
          // Si lo que viene tras el corchete no es la palabra clave, error.
          accion[0] = NULLCHAR;
          i = 0;
          return res;
        }
      }
    }

    else if(arrayStarted && !arrayEnded)
    {
      //Mientras el array haya comenzado y no se haya encontrado el final, recibimos las acciones
      if(startrcv && !endrcv)
      {
        // Si hemos encontrado el inicio de una accion y no el final, almacenamos
        accion[i++] = *read;
      }

      if(*read == LLA)
      {
        // Si viene una llave abierta, indica principio de una acción
        startrcv = true;
      }
      else if(startrcv && *read == LLC)
      {
        // Si ya hemos detectado el principio de una acción y viene una llave cerrada, es el fin de la acción.
        endrcv = true;
        accion[i-1] = NULLCHAR; //Borramos la llave final
      }
      else if(*read == CC)
      {
        //Si encontramos un corchete cerrado, encontramos el fin del array.
        arrayEnded=true;
        accion[0] = NULLCHAR;
        i = 0;
      }

      if(startrcv && endrcv)
      {
        // Se ha detectado el inicio y el fin de una accion, la procesamos.
        res = procesaAccion(accion) || res;
        accion[0] = NULLCHAR;
        i = 0;
        startrcv = false;
        endrcv = false;
      }
    }
    else{
      // Hemos encontrado principio y fin del array de acciones. Terminamos ignorando el resto.
      accion[0] = NULLCHAR;
      i = 0;
      return res;
    }
  }
  while(*read++);

  res = getNumSerieReq() || res;

  return res;
}

boolean procesaAccion(char *accionFull)
{

  // accionFull contiene una sola acción en la forma
  // accion:orden,valor1:valor,valor2:valor
  boolean res = true;
  char keys[MAX_ARGUMENTS][3];
  char values[MAX_ARGUMENTS][6];
  char *accion;
  char *key;
  char *value;
  int i = 0;

  if(accionFull == NULL)
  {
    return false;
  }

  while(((accion = strsep(&accionFull, COMA)) != NULL) && i < MAX_ARGUMENTS)
  {
    //Lo primero que hacemos es romper la acción por comas, de este modo separamos la acción a ejecutar, el valor1 asociado y el valor2 asociado.
    if((key = strsep(&accion, DOSPUNTOS)) != NULL){
      //Dentro de cada trozo, la clave será lo de antes de los : dos puntos
      // El valor lo de después
      value = strsep(&accion, DOSPUNTOS);
    }

    if(key == NULL || value == NULL)
    {
      res = false;
      break;
    }

    if(strcmp(value, NULLS) != 0)
    {
      //Almacenamos en un array las keys y los values
      // Las keys DEBEN ser accion, value1 y value2
      strcpy(keys[i], key);
      // Los valores serán la acción a ejecutar y sus valores si los hay.
      strcpy(values[i], value);
      i++;
    }
  }

  if(res)
  {
    // Si todo ha ido bien, ejecutamos la acción.
    res = ejecutaAccion(keys, values, i);
  }

  return res;
}

boolean ejecutaAccion(char keys[][3], char values[][6], int n)
{
  boolean res = false;
  int caseType = -1;

  if(n > MAX_ARGUMENTS)
  {
    // Demasiados parametros
    return false;
  }

  if(strcmp(keys[0], ACTION) != 0)
  {
    // Si la primera clave no es "acción", el arduino no entiende la petición
    return false;
  }

  // El primera valor indicará la acción, que esta mapeada con un número. 
  caseType = atoi(values[0]);

  if(caseType >=2 && caseType <= 7 && strcmp(mode, MODE_AUTO) == 0)
  {
    // Si se requiere activar/desactivar los subsistemas y el modo esta en auto
    // hay un error. Está prohibido.
    caseType = 40;
  }

  switch (caseType)
  {		
    // Ejecutamos una acción u otra, dependiendo de lo que se pida.
  case SWITCHMODE:
    if(strcmp(keys[1],VALUE1) == 0 && n  >= 2)
      res = switchModeReq(values[1]);
    break;
  case GETDATA:
    res = getDataReq();
    break;
  case ENCENDERRIEGO:
    res = encenderRiegoReq();
    break;
  case ENCENDERLUZ:
    res = encenderLuzReq();
    break;
  case ENCENDERTEMP:
    res = encenderTempReq();
    break;
  case APAGARRIEGO:
    res = apagarRiegoReq();
    break;
  case APAGARLUZ:
    res = apagarLuzReq();
    break;
  case APAGARTEMP:
    res = apagarTempReq();
    break;
  case GETLUMMINMAX:
    res = getLumMinMaxReq();
    break;
  case GETTEMPMINMAX:
    res = getTempMinMaxReq();
    break;
  case GETHUMMINMAX:
    res = getHumMinMaxReq();
    break;
  case GETMAXPULSOSRIEGO:
    res = getMaxPulsosRiegoReq();
    break;
  case GETLUMHIST:
    res = getLumHistReq();
    break;
  case GETFOTOPERIODO:
    res = getFotoPeriodoReq();
    break;
  case GETHORARESET:
    res = getHoraResetReq();
    break;
  case SETLUMMINMAX:
    if(strcmp(keys[1],VALUE1) == 0 && strcmp(keys[2],VALUE2) == 0 && n >= 3)
      res = setLumMinMaxReq(values[1], values[2]);
    break;
  case SETTEMPMINMAX:
    if(strcmp(keys[1],VALUE1) == 0 && strcmp(keys[2],VALUE2) == 0 && n >= 3)
      res = setTempMinMaxReq(values[1], values[2]);
    break;
  case SETHUMMINMAX:
    if(strcmp(keys[1],VALUE1) == 0 && strcmp(keys[2],VALUE2) == 0 && n >= 3)
      res = setHumMinMaxReq(values[1], values[2]);
    break;
  case SETMAXPULSOSRIEGO:
    if(strcmp(keys[1],VALUE1) == 0 && n >= 2)
      res = setMaxPulsosRiegoReq(values[1]);
    break;
  case SETLUMHIST:
    if(strcmp(keys[1],VALUE1) == 0 && n >= 2)
      res = setLumHistReq(values[1]);
    break;
  case SETFOTOPERIODO:
    if(strcmp(keys[1],VALUE1) == 0 && n >= 2)
      res = setFotoPeriodoReq(values[1]);
    break;
  case SETHORARESET:
    if(strcmp(keys[1],VALUE1) == 0 && strcmp(keys[2],VALUE2) == 0 && n >= 3)
      res = setHoraResetReq(values[1], values[2]);
    break;
  case GETNUMSERIE:
    res = getNumSerieReq();
    break;
  case DELAY:
    // Caso especial. Se usa para meter esperas entre pulsos de riego, para activar varios pulsos en una sola vez.
    if(strcmp(keys[1],VALUE1) == 0 && n >= 2)
      res = getDelay(values[1]);
    break;
  case NOP:
    // Caso especial. Si no hay nada que hacer se trata de un NOP y no hacemos nada.
    res = getNOP();
    break;
  case MODEAUTOISON:
    // Caso especial. Se envia un error de acción prohibida.
    res = autoModeEnabledReq();
    break;
  default:
    // Si no es ninguno de los casos anteriores, error de sintáxis.
    res = badActionReq();
    break;
  }

  return res;
}

/*
Métodos que ejecutan acciones sobre el sistema. 
 Cada uno de ellos ejecuta una acción a la vez que rellena el buffer HTTP_to_server con la información del resultado de la ejecución.
 Si todo va bien devuelven true. Si se trata del informe de un error, devuelven false.
 
 Tras su ejecución HTTP_to_server contendrá un JSON bien formado con el estado del sistema, que será enviado de nuevo al servidor para informarle.
 */

boolean autoModeEnabledReq()
{
  char buffer[60];
  strcpy_P(buffer, (char*)pgm_read_word(&(errores[2])));
  strcat(HTTP_to_server, buffer);
  //strcpy(HTTP_resp_code, CODE_FORBIDDEN);
  return false;
}

boolean badSyntaxReq()
{
  Serial.println(F("Error 500"));
  char buffer[60];
  strcpy_P(buffer, (char*)pgm_read_word(&(errores[0])));
  strcat(HTTP_to_server, buffer);
  return false;
}

boolean badActionReq()
{
  char buffer[60];
  strcpy_P(buffer, (char*)pgm_read_word(&(errores[4])));
  strcat(HTTP_to_server, buffer);
  return false;
}

boolean switchModeReq(char newMode[])
{
  strcpy(mode, newMode);
  apagarLuz();
  apagarRiego();
  apagarTemp();

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, NUEVOMODO);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, COMILLAS);
  strcat(HTTP_to_server, newMode);
  strcat(HTTP_to_server, COMILLAS);
  finalizaResp();

  return true;
}

boolean getNumSerieReq()
{
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, NUMSERIE);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, NUM_SERIE);
  finalizaResp();

  return true;
}

boolean getDelay(char value[])
{
  delay(atoi(value));
  if(riegoActivo == 1)
  {
    pulsosAcumulados ++;
  }
  return true;
}

boolean getNOP()
{
  return true;
}

boolean getDataReq()
{
  char aux[7];

  iniciaResp();

  itoa(luminosidad, aux, 10);
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUMINOSIDAD);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  itoa(humedad, aux, 10);
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, HUMEDAD);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  itoa(temperatura, aux, 10);
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPERATURA);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);

  finalizaResp();

  iniciaResp();

  itoa(luzActiva, aux, 10);
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUZACTIVA);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  itoa(riegoActivo, aux, 10);
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, RIEGOACTIVO);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  itoa(tempActiva, aux, 10);
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPACTIVA);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);

  finalizaResp();

  return true;
}

boolean encenderRiegoReq()
{
  encenderRiego();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, RIEGOACTIVO);
  char aux[2];
  itoa(riegoActivo, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean encenderLuzReq()
{
  encenderLuz();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUZACTIVA);
  char aux[2];
  itoa(luzActiva, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean encenderTempReq()
{
  encenderTemp();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPACTIVA);
  char aux[2];
  itoa(tempActiva, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean apagarRiegoReq()
{
  apagarRiego();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, RIEGOACTIVO);
  char aux[2];
  itoa(riegoActivo, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean apagarLuzReq()
{
  apagarLuz();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUZACTIVA);
  char aux[2];
  itoa(luzActiva, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean apagarTempReq()
{
  apagarTemp();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPACTIVA);
  char aux[2];
  itoa(tempActiva, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean getLumMinMaxReq()
{
  char aux[7];

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUMMIN);
  itoa(lumMin, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUMMAX);
  itoa(lumMax, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean getHumMinMaxReq()
{
  char aux[7];

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, HUMMIN);
  itoa(humMin, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, HUMMAX);
  itoa(humMax, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean getTempMinMaxReq()
{
  char aux[7];

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPMIN);
  itoa(tempMin, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPMAX);
  itoa(tempMax, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean getMaxPulsosRiegoReq()
{
  char aux[7];

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, MAXPULSOSRIEGO);
  itoa(maxPulsosRiego, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean getLumHistReq()
{
  char aux[7];

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUMHIST);
  int auxInt = lumHist * 100;
  itoa(auxInt, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean getFotoPeriodoReq()
{
  char aux[7];

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, FOTOPERIODO);
  itoa(fotoPeriodo, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean getHoraResetReq()
{
  char aux[7];

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, HORARESET);
  itoa(hora_reset, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, MINUTOSRESET);
  itoa(minutos_reset, aux, 10);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, aux);
  finalizaResp();

  return true;
}

boolean setLumMinMaxReq(char valueMin[], char valueMax[])
{
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUMMIN);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueMin);
  finalizaResp();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUMMAX);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueMax);
  finalizaResp();

  lumMin = atoi(valueMin);
  lumMax = atoi(valueMax);

  return true;
}

boolean setHumMinMaxReq(char valueMin[], char valueMax[])
{
  humMin = atoi(valueMin);
  humMax = atoi(valueMax);

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, HUMMIN);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueMin);
  finalizaResp();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, HUMMAX);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueMax);
  finalizaResp();

  return true;
}

boolean setTempMinMaxReq(char valueMin[], char valueMax[])
{
  tempMin = atoi(valueMin);
  tempMax = atoi(valueMax);

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPMIN);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueMin);
  finalizaResp();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, TEMPMAX);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueMax);
  finalizaResp();

  return true;
}

boolean setMaxPulsosRiegoReq(char value[])
{
  maxPulsosRiego = atoi(value);

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, MAXPULSOSRIEGO);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, value);
  finalizaResp();

  return true;
}

boolean setLumHistReq(char value[])
{
  int aux = atoi(value); 
  lumHist = aux * 0.01;

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, LUMHIST);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, value);
  finalizaResp();

  return true;
}

boolean setFotoPeriodoReq(char value[])
{
  fotoPeriodo = atoi(value);

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, FOTOPERIODO);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, value);
  finalizaResp();

  return true;
}

boolean setHoraResetReq(char valueH[], char valueM[])
{
  hora_reset = atoi(valueH);
  minutos_reset = atoi(valueM);

  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, HORARESET);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueH);
  finalizaResp();
  iniciaResp();
  strcat(HTTP_to_server, ELEMENTO);
  strcat(HTTP_to_server, MINUTOSRESET);
  strcat(HTTP_to_server, VALOR);
  strcat(HTTP_to_server, valueM);
  finalizaResp();

  return true;
}

// Auxiliar para evitar repeticiones.
// Construye el inicio del JSON de respuesta.
void iniciaResp()
{
  strcat(HTTP_to_server, INIT_RESP);	
}

// Auxiliar para evitar repeticiones.
// Construye el final del JSON de respuesta.
void finalizaResp()
{
  strcat(HTTP_to_server, END_RESP);
  strcat(HTTP_to_server, COMA);
  //strcpy(HTTP_resp_code, CODE_OK);
}

// En el modo automático se encarga de la gestión de los subsistemas
void gestion(long tiempoms)
{
  gestionLuz(tiempoms);
  gestionHumedad();
  gestionTemp();
}

void gestionLuz(long tiempoms)
{
  if(luzActiva || luminosidad < lumMin)
  {
    // Si la luz está encendida, o la luminosidad está por encima del límite, actualizamos el valor de la luz acumulada.
    luzAcumulada += tiempoms;
  }

  if(strcmp(mode, MODE_AUTO) == 0)
  {
    // Si el modo es automático.
    if(luzAcumulada < fotoPeriodo && luminosidad > lumMax * (1 + lumHist))
    {
      // Si la luz acumulada es inferior  al fotoperiodo de la planta
      // y la luminosidad es más baja que un porcentaje aceptable del límite establecido
      // encendemos la luz.
      // lumHist es la histéresis, el porcentaje que se considera aceptable con respecto al límite.
      // El sensor de luminosidad informa inversamente. Un valor alto indica poca luminosidad. Un valor bajo, mucha.
      encenderLuz();
    }
    else if (luzAcumulada >= fotoPeriodo || luminosidad < lumMin * (1 - lumHist))
    {
      // Si la luz acumulada es mayor que el fotoperiodo o la luminosidad actual (con luz natural) está por encima del límite
      // apagamos la luz.
      apagarLuz();
    }
  }
}

void gestionHumedad()
{
  // La gestion del riego es especial. A diferencia de otros sistemas, que se encienden o se apagan de un modo continuo
  // el riego se activa durante un pulso (un periodo corto de tiempo) y se apaga. Esto evita que ante cualquier fallo el 
  // riego permanezca encendido inundandolo todo.
  if(strcmp(mode, MODE_AUTO) == 0)
  {
    if(humedad < humMin && pulsosAcumulados < maxPulsosRiego)
    {
      // Si la humedad es demasiado baja y no hemos regado lo suficente
      // encendemos el riego.
      encenderRiego();
      // Lo mantenemos encendido durante la duración de un pulso
      delay(DURACION_PULSO_RIEGO);
      // Y apagamos.
      apagarRiego();
      pulsosAcumulados++;
    }
  }
}

void gestionTemp()
{
  if(strcmp(mode, MODE_AUTO) == 0)
  {
    if(temperatura <= tempMin || temperatura >= tempMax)
    {
      // Encendemos la alerta de temperatura (o sistema de control asociado) si estamos
      // por debajo del minimo o por encima del maximo.
      encenderTemp();
    } 
    else
    {
      // En otro caso, apagamos.
      apagarTemp();
    }
  }
}

void encenderRiego()
{
  digitalWrite(riego, HIGH);
  Serial.println(F("Riego: 1"));
  riegoActivo = 1;
}

void encenderLuz() 
{
  digitalWrite(luz, HIGH);
  Serial.println(F("Luz: 1"));
  luzActiva = 1;
}

void encenderTemp()
{
  digitalWrite(tempAlert, HIGH);
  Serial.println(F("Alerta temp: 1"));
  tempActiva = 1;
}

void apagarRiego()
{
  digitalWrite(riego, LOW);
  Serial.println(F("Riego: 0"));
  riegoActivo = 0;
}

void apagarLuz(){  
  digitalWrite(luz, LOW);
  Serial.println(F("Luz: 0"));
  luzActiva = 0;
}

void apagarTemp()
{
  digitalWrite(tempAlert, LOW);
  Serial.println(F("Alerta temp: 0"));
  tempActiva = 0;
}

void leeHumTemp()
{
  //Lectura de los sensores de humedad y temperatura
  humedad = dht.readHumidity();
  temperatura = dht.readTemperature();

  Serial.print(F("Humedad: "));
  Serial.print(humedad);
  Serial.println(F("(%)."));
  Serial.print(F("Temperatura: "));
  Serial.print(temperatura);
  Serial.println(F("(C)."));

  delay(2000);
}

void leeLuminosidad()
{
  // Lectura del sensor de luminosidad
  Serial.print(F("Luminosidad: "));
  luminosidad = analogRead(senLum);
  Serial.println(luminosidad);
} 

// Helper function for free ram.
// With use of http://playground.arduino.cc/Code/AvailableMemory
int freeRam(void)
{
  extern unsigned int __heap_start;
  extern void *__brkval;

  int free_memory;
  int stack_here;

  if (__brkval == 0)
    free_memory = (int) &stack_here - (int) &__heap_start;
  else
    free_memory = (int) &stack_here - (int) __brkval;

  return (free_memory);
}

/*-------- NTP code ----------*/
// Método encargado de la sincronización del reloj de arduino
// Ejemplo de la libreria TIME adaptado para arduino v1.X
unsigned long getNtpTime()
{
  sendNTPpacket(timeServer); // send an NTP packet to a time server
  delay(1000);
  if ( Udp.parsePacket() ) { 
    Udp.read(packetBuffer,NTP_PACKET_SIZE);  // read packet into buffer


    //the timestamp starts at byte 40, convert four bytes into a long integer
    unsigned long hi = word(packetBuffer[40], packetBuffer[41]);
    unsigned long low = word(packetBuffer[42], packetBuffer[43]);
    // this is NTP time (seconds since Jan 1 1900
    unsigned long secsSince1900 = hi << 16 | low;  
    // Unix time starts on Jan 1 1970
    const unsigned long seventyYears = 2208988800UL;     
    unsigned long epoch = secsSince1900 - seventyYears;  // subtract 70 years
    return epoch;
  }
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(IPAddress address)
{
  memset(packetBuffer, 0, NTP_PACKET_SIZE);  // set all bytes in the buffer to 0

  // Initialize values needed to form NTP request
  packetBuffer[0] = B11100011;   // LI, Version, Modxe
  packetBuffer[1] = 0;     // Stratum
  packetBuffer[2] = 6;     // Max Interval between messages in seconds
  packetBuffer[3] = 0xEC;  // Clock Precision
  // bytes 4 - 11 are for Root Delay and Dispersion and were set to 0 by memset
  packetBuffer[12]  = 49;  // four-byte reference ID identifying
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // send the packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer,NTP_PACKET_SIZE);
  Udp.endPacket();
}

/*
// Se atiende la red en modo servidor.
 void atenderRed(){
 boolean res = false;
 char* acciones;
 
 client = server.available();
 
 if (client) {
 Serial.println(F("Cliente encontrado. Atendiendo http..."));
 
 boolean currentLineIsBlank = true;
 boolean bodyrcv = false;
 int n = 0;
 boolean startrcv = false;
 boolean endrcv = false;
 
 while (client.connected()) {
 if (client.available()) {   // client data available to read
 char c = client.read(); // read 1 byte (character) from client      
 
 // CONTROL //
 if(c == '\n' && currentLineIsBlank)
 {
 bodyrcv = true;
 }
 // every line of text received from the client ends with \r\n (at least in header)
 else if (c == '\n') {
 // last character on line of received text
 // starting new line with next character read
 currentLineIsBlank = true;
 } 
 else if (bodyrcv && c != '\r' && !endrcv) {
 // a text character was received from client
 if(c == '[')
 {
 n++;
 startrcv = true;
 }
 else if (c == ']' && startrcv)
 {
 n--;
 if(n == 0)
 {
 endrcv = true;
 }
 }
 }
 
 // ALMACENAMIENTO //
 if(startrcv && !endrcv) //Si todavia no hemos recibido la orden, la almacenamos
 {
 if(bufIndex < FROM_SIZE) {
 if(c != ' ' && c != '"' && c != '\r' && c != '\n' && c != '\t' && c != '[' && c != ']')
 {
 HTTP_from_server[bufIndex] = c; 
 bufIndex++;
 }
 } 
 else {
 HTTP_from_server[bufIndex]='\0';
 while(client.read());  
 HTTP_to_server[0] = '[';
 strcat(HTTP_to_server, ERROR_OVERFLOW); 
 char *rem = HTTP_to_server; //Para quitar la coma final
 rem[strlen(rem)-1] = '\0';
 strcat(HTTP_to_server, "]");
 sendResponseBadRequest(client);
 Serial.println(HTTP_from_server);
 Serial.println(HTTP_to_server);
 HTTP_from_server[0]='\0';
 HTTP_to_server[0]='\0';
 bufIndex = 0; // finished with request, index to 0
 break;
 }
 }
 
 // RESPUESTA //
 // last line of client request is blank and ends with \n
 // respond to client only after last line received
 if (bodyrcv && startrcv && endrcv) {
 HTTP_from_server[bufIndex]='\0';
 if(HTTP_from_server[0] != '\0'){
 HTTP_to_server[0] = '[';
 res = procesaAcciones(HTTP_from_server);
 char *rem = HTTP_to_server; //Para quitar la coma final
 rem[strlen(rem)-1] = '\0';
 strcat(HTTP_to_server, "]");
 }
 
 if(res)
 {
 sendResponseOK(client);
 }
 else
 {  
 HTTP_to_server[0] = '[';
 strcat(HTTP_to_server, ERROR_BAD_SYNTAX);
 char *rem = HTTP_to_server; //Para quitar la coma final
 rem[strlen(rem)-1] = '\0';
 strcat(HTTP_to_server, "]");
 sendResponseBadRequest(client);
 }
 Serial.println(HTTP_to_server);
 HTTP_from_server[0]='\0';
 HTTP_to_server[0]='\0';
 bufIndex = 0; // finished with request, index to 0
 break;
 }
 } // end if (client.available())
 else{ // La peticion termina y no encaja con ningun patron: Bad request
 HTTP_from_server[bufIndex]='\0';
 HTTP_to_server[0] = '[';
 strcat(HTTP_to_server, ERROR_BAD_SYNTAX); 
 char *rem = HTTP_to_server; //Para quitar la coma final
 rem[strlen(rem)-1] = '\0';
 strcat(HTTP_to_server, "]");
 sendResponseBadRequest(client);
 Serial.println(HTTP_to_server);
 HTTP_from_server[0]='\0';
 HTTP_to_server[0]='\0';
 bufIndex = 0; // finished with request, index to 0
 break;
 }
 } // end while (client.connected())
 
 delay(1);      // give the web browser time to receive the data
 client.stop(); // close the connection
 } // end if (client)
 }
 */

/*
void sendResponseOK(EthernetClient client)
 {
 // send a standard http response header
 client.println(HTTP_resp_code);
 client.println(HTTP_RESP_HEADER);
 client.println(HTTP_HEADER_CON_CLOSE);
 client.println();
 // send json
 client.print(HTTP_to_server);
 }
 
 void sendResponseBadRequest(EthernetClient client)
 {
 // send a standard http response header
 client.println(CODE_BAD_REQ);
 client.println(HTTP_RESP_HEADER);
 client.println(HTTP_HEADER_CON_CLOSE);
 client.println();
 // send json
 client.print(HTTP_to_server);  
 }
 */








